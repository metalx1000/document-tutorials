#!/bin/bash

echo "Creating salaries.csv..."

#create 100 entries
for i in {1..100}
do
  #select random person's name
  shuf -n 1 people.csv|cut -d\, -f2,3|tr -d "\n" 
  echo -n ","
  #create 25 years of sallaries
  for i in {1..25};do echo -n "\"\$$(shuf -i 30000-120000 -n 1| sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')\",";done

  #add new line after each person
  echo ""
done > salaries.csv

echo "Complete."
