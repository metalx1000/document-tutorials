#!/bin/bash

echo "Creating salaries.html"

cat <<'EOF' > salaries.html
<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </head>
  <body>
    <div id="main" class="container">
      <div class="row">
EOF

cat salaries.csv|while read line;
do 
  name="$(echo "$line"|cut -d\| -f1)"
  avg="$(echo "$(echo "($(echo "$line"|head -n 1|tr "|" "\n"|\
    grep "[0-9]"|\
    sed 's/\$//g;s/,//g'|\
    sort -g|\
    tail -n 5|\
    tr "\n" "+")0)/5*.75"|bc)")"

  y="$(echo "Yearly: \$$avg"|sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')"
  m="$(echo "Monthly: \$$(echo "$avg/12"|bc)"|sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')"
  echo "<div class='card' style='width: 18rem;'><div class='card-body'><h5 class='card-title'>$name</h5><p class='card-text'>$y<br>$m</p></div></div>" 

done >> salaries.html

cat <<EOE >> salaries.html

</div>
</div>

</body>
</html>
EOE

xdg-open salaries.html
