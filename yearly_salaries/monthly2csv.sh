#!/bin/bash

echo "Creating js.csv..."
cat salaries.csv|while read line;
do 
  echo "$line"|cut -d\| -f1|tr "\n" "|"
  avg="$(echo "$(echo "($(echo "$line"|tr "|" "\n"|\
    grep "[0-9]"|\
    sed 's/\$//g;s/,//g'|\
    sort -g|\
    tail -n 5|\
    tr "\n" "+")0)/5*.75"|bc)")"

  echo "\$$avg"|sed ':a;s/\B[0-9]\{3\}\>/,&/;ta'|tr "\n" "|"
  echo "\$$(echo "$avg/12"|bc)"|sed ':a;s/\B[0-9]\{3\}\>/,&/;ta'
done > js.csv

echo "done."
